#include "rs_encode.h"
#include <iostream>
#include <fstream>
#include <array>
#include <random>
#include "ccsds.h"
#include "fixed.h"

using namespace std;
//random_device r;
default_random_engine generator(42);
uniform_int_distribution<int> randint(0,255);
void rs_encode(std::array<data_t, RS_DATA_LEN> &data_in, std::array<data_t, RS_BLOCK_LEN> &data_out, bool use_dual_basis);


int main()
{
//	// Open files where to save results
//	ofstream INPUT;
//	ofstream DUT;
//	INPUT.open ("input.dat");
//	DUT.open ("dut_ouput.dat");
//
//    array<array<int,MESSAGE_LENGTH>, NUMLINES> input;
//    array<array<int,ENCODED_LENGTH>, NUMLINES> dut_output;
//    array<int,MESSAGE_LENGTH> temp1;
//    array<int,ENCODED_LENGTH> temp2;
//
//	// Create input data
//	for(int i=0; i<NUMLINES;i++) {
//		for(int j=0; j<MESSAGE_LENGTH; j++){
//			int number = randint(generator);
//		    temp1[j] = number;
//		}
//		input[i] = temp1;
//	}
//
//	// dump input vectors into a .dat file for later use
//    for (const auto& line : input)
//    {
//        for (const auto& x : line)
//        {
//        	INPUT << " " << x;
//        }
//        INPUT << endl;
//    }
//    INPUT.close();
//
//    // Get golden dat file
//    // TODO
//    // For now, just copy the dut
//
//    // get output from DUT
//	for(int i=0; i<NUMLINES;i++) {
//		dut_output[i] = rs_encode(input[i]);
//	}
//
//	// dump DUT vectors into a .dat file for later use
//    for (const auto& line : dut_output)
//    {
//        for (const auto& x : line)
//        {
//        	DUT << " " << x;
//        }
//        DUT << endl;
//    }
//    DUT.close();
//
//    //print some cases for verification
//    if (MESSAGE_LENGTH > 10){
//		for(int line=0; line<10; line++) {
//			cout << "Input : " << endl;
//			for (const auto& x : input[line])
//			{
//				cout << " " << x;
//			}
//			cout << endl;
//
//			cout << "DUT : " << endl;
//			for (const auto& x : dut_output[line])
//			{
//				cout << " " << x;
//			}
//			cout << endl;
//
//		}
//    }
//    else{
//		cout << "Input : " << endl;
//		for (const auto& x : input[0])
//		{
//			cout << " " << x;
//		}
//		cout << endl;
//
//		cout << "DUT : " << endl;
//		for (const auto& x : dut_output[0])
//		{
//			cout << " " << x;
//		}
//		cout << endl;
//    }
//
//    //check that the dut_output matches the golden data
//    int diff_retval = system("diff --brief -w dut_ouput.dat dut_ouput.dat");
//    if (diff_retval != 0) {
//      cout << "Test failed  !!!" << endl;
//      diff_retval=1;
//    } else {
//      cout << "Test passed !" << endl;
//    }
//
//    return diff_retval;


//// CRUDE TEST

	array<data_t,MESSAGE_LENGTH> temp1;
	array<data_t,ENCODED_LENGTH> temp2;

	for(int j=0; j<MESSAGE_LENGTH; j++){
		data_t number = (data_t) randint(generator);
		temp1[j] = number;
	}

	cout << "INPUT : " << endl;
	for (const auto& x : temp1)
	{
		cout << " " << (int) x;
	}
	cout << endl;

	rs_encode(temp1, temp2, false);

	// Parity should be 138 154 142 218 171 233 235 91 227 77 158 243 105 58 71 132 36 129 147 92 151 248 227 207 229 173 174 255 253 17 82 181

	cout << "DUT : " << endl;

	for (const auto& x : temp2)
	{
		cout << " " << (int) x;
	}
	cout << endl;
  return 0;
}
