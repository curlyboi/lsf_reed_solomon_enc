with open("input.dat") as f:
    lines = f.readlines()
    L = []
    for i, line in enumerate(lines):
        l = line.split(' ')
        l.remove('')
        l[-1] = l[-1][:-1]
        L.append([int(x) for x in l])

with open("cleaned_input.dat", "wb") as f:
    for l in L:
        for x in l:
            f.write(x.to_bytes(8, "big"))
        f.write('\n'.encode('ascii'))
