/* -*- c++ -*- */
/*
 * Copyright 2021 Baptiste Fournier.
 * Based on GNU GPLv3 gr-ccsds library from André Løfaldli.
 * Based on GNU LGPL code from Phil Karn, KA9Q.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#include "rs_encode.h"
#include "ccsds.h"
#include "fixed.h"
#include "cstring"
#include "array"
#undef A0
#define A0 (NN) /* Special reserved value encoding zero in index form */

extern unsigned char ALPHA_TO[];
extern unsigned char INDEX_OF[];
extern unsigned char GENPOLY[];
extern unsigned char DATADUAL[];
extern unsigned char PARITYDUAL[];

static inline int mod255(int x){ //weird modulo, but seems good after test
  while (x >= 255) {
    x -= 255;
    x = (x >> 8) + (x & 255);
  }
  return x;
}

void rs_encode(std::array<data_t, RS_DATA_LEN> &data_in, std::array<data_t, RS_BLOCK_LEN> &data_out, bool use_dual_basis){
#pragma HLS INTERFACE axis port=data_out register_mode=both register
#pragma HLS INTERFACE axis port=data_in register_mode=both register
#pragma HLS TOP name=rs_encode
#pragma HLS INTERFACE s_axilite port=use_dual_basis
	std::array<data_t, RS_PARITY_LEN> parity;
#pragma HLS BIND_STORAGE variable=parity type=ram_t2p
#pragma HLS ARRAY_PARTITION variable=parity dim=1 factor=16 block
    parity.fill(0);
	data_t feedback;

    dataProcessingLoop: for(int i=0;i<RS_DATA_LEN;i++){
    	// DUAL BASIS CONVERSION FOR DATA
		if (use_dual_basis) {
			data_out[i] = DATADUAL[data_in[i]];
		}
		else{
			data_out[i] = data_in[i];
		}
		//PARITY COMPUTATION
		feedback = INDEX_OF[data_in[i] ^ parity[0]];
		if(feedback != A0){
			encodeFeedbackLoop: for(int j=1;j<NROOTS;j++){
#pragma HLS UNROLL
				parity[j] ^= ALPHA_TO[mod255(feedback + GENPOLY[NROOTS-j])]; // weird modulo version
			}
			std::memmove(&parity[0],&parity[1],sizeof(data_t)*(NROOTS-1));
			parity[NROOTS-1] = ALPHA_TO[mod255(feedback + GENPOLY[0])];
		}
		else{
			std::memmove(&parity[0],&parity[1],sizeof(data_t)*(NROOTS-1));
			parity[NROOTS-1] = 0;
		}
    }

    // DUAL BASIS CONVERSION FOR PARITY
    convertParityDual: for(int i=0; i<RS_PARITY_LEN; i++){
    	if (use_dual_basis) {
    		data_out[RS_DATA_LEN+i] = PARITYDUAL[parity[i]];
    	}
    	else{
    		data_out[RS_DATA_LEN+i] = parity[i];
    	}
	}
}
